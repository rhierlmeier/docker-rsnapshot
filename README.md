Rsnapshot Backup with Docker
============================

This git repository contains a Dockerfile for a Dockerimage that
executes a rsnapshot backup from the /data volume to the /backup volume.

To build the image run the following command:

```bash
docker build -t "rhierlmeier/rsnapshot" .
```

To start a container execute the following command:

```bash
docker run \
       -v $PWD/test/data:/data \
       -v $PWD/test/backup/logs:/logs \
       -v $PWD/test/backup/snapshots:/snapshots \
       --name <container_name> --detach rhierlmeier/rsnapshot
```

Please replace `<container_name>` with your preferred name of the docker container. 

The container uses the following volumns:

/data       Directory with the data that should be backuped
/logs       Directory where the logs of rsnapshot are stored
/snapshots  In this directory the backup is created

Start a manual backup
---------------------

The following command executes a daily backup:

```
docker exec <container_id_or_name> /usr/bin/rsnapshot -v daily
```

Backup of a MySql database (optional)
-------------------------------------

Rsnapshot can execute during a backup shell script to perform additional 
actions. This GIT repo contains the shell script `mysql_backup.sh` as sample.
To create a backup of a MySql DB perform the following steps:

1. Adjust the mysql-backup.sh script:

```bash
#!/bin/sh
# ---------------------------------------
# Creates a mysql dump for any mysql container
# that is linked to this container
# ---------------------------------------

dbhost=mysqlhost
dbuser=mysqluser
dbpassword=secret
dbbackupfile=mysql-backup.sql
dbname=sampledb

mysqldump -h "$dbhost" \
             -u "$dbuser" \
             -p "$dbpassword" \
             -r "$dbbackupfile" \
             --lock-tables \
             "$dbname"
gzip "$dbbackupfile"

exit 0
```
1. Adjust the file rsnapshot.conf. Uncomment the following line
```
backup_script	/mysql_backup.sh	dbbackups/
```
1. Build the image (the change the files are copied during the build into the image:
```bash
docker build -t "rhierlmeier/rsnapshot"
```
1. Run the container and observer the output





