#!/bin/sh
# ---------------------------------------
# Creates a mysql dump for any mysql container
# that is linked to this container
# ---------------------------------------

dbhost=mysqlhost
dbuser=mysqluser
dbpassword=secret
dbbackupfile=mysql-backup.sql
dbname="sample"

mysqldump -h "$dbhost" \
             -u "$dbuser" \
             -p "$dbpassword" \
             -r "$dbbackupfile" \
             --lock-tables \
             "$dbname"
gzip "$dbbackupfile"

exit 0