FROM alpine

RUN apk add --update --no-cache rsnapshot mysql-client

COPY tasks/ /etc/periodic/
copy rsnapshot.conf /etc
copy mysql_backup.sh /

RUN chmod -R +x /etc/periodic/ && \
    chmod 755 /mysql_backup.sh


CMD ["crond", "-f", "-d", "8"]
